<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');


    //Animal 
    $shaun = new Animal("Shaun");

    echo "Name : " . $shaun->name . "<br>" ; 
    echo "Legs : " . $shaun->legs . "<br>" ; 
    echo "Cold Blooded : " . $shaun->cold_blooded  . "<br>" ;

    echo "<br>";

    //frog
    $frog = new Frog("buduk");

    echo "Name : " . $frog->name . "<br>" ; 
    echo "Legs : " . $frog->legs . "<br>" ; 
    echo "Cold Blooded : " . $frog->cold_blooded  . "<br>" ;
    echo $frog->Jump("Hop Hop") . "<br>";

    echo "<br>";

    //ape
    $ape = new Ape("Kera Sakti");

    echo "Name : " . $ape->name . "<br>" ; 
    echo "Legs : " . $ape->legs . "<br>" ; 
    echo "Cold Blooded : " . $ape->cold_blooded  . "<br>" ;
    echo $ape->Yell("Auooooooo") . "<br>";

?>
